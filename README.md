# Examples

## Example 1

---

Login into your AWS Console, navigate to the EC2 console and apply the configration below.

### Create a ssh key pair

Create a key pair with the name **mykey** and save the private key on your computer.

### Security group

Create a security group with the name **webserver**
Add a rule that allows http traffic from the source 0.0.0.0/0.

### EC2 instance

Create an EC2 instance with the AMI **Amazon Linux 2** of type **t2.micro**. In the Advanced Details section add the user-data payload below as text.

#### User-Data payload

```text
#!/usr/bin/env bash

sudo yum install -y httpd
sudo systemctl start httpd
sudo systemctl enable httpd
```

Accept the defaults for **Storage** and **Tags**.

In the **Security Groups** tab choose "Select existing security group" and check the group that you created before.

**Review and Launch your instance**. When asked select the key you have created.

## Example 2

---

[Amazon EC2 Autoscaling Example](https://docs.aws.amazon.com/autoscaling/ec2/userguide/GettingStartedTutorial.html#gs-create-lt)

## Example 3

---

[Hosting static content on S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)

```text
{
    "Version": "2008-10-17",
    "Id": "PolicyForPublicWebsiteContent",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::owd/*"
        }
    ]
}
```

```text
<!doctype html>
<html>
  <head>
    <title>Static website hosting with S3</title>
  </head>
  <body>
    <p>My awesome website!</p>
  </body>
</html>
```
